# microfluidics_for_underfill


* Study of underfill flow in semiconductor packaging process
[1] L.C. Stencel, J. Strogies, B. Müller, R. Knofe, C. Borwieck, M. Heimann, Capillary Underfill Flow Simulation as a Design Tool for Flow-Optimized Encapsulation in Heterogenous Integration, Micromachines 14 (2023) 1885. https://doi.org/10.3390/mi14101885.
[2] F.C. Ng, A. Abas, M.Z. Abdullah, M.H.H. Ishak, G.Y. Chong, Comparative Study of the Scaling Effect on Pressure Profiles in Capillary Underfill Process, IOP Conf. Ser.: Mater. Sci. Eng. 203 (2017) 012012. https://doi.org/10.1088/1757-899X/203/1/012012.
[3] A. Abas, H. M.S., M.H.H. Ishak, N. A.S., M.Z. Abdullah, F. Che Ani, Effect of ILU dispensing types for different solder bump arrangements on CUF encapsulation process, Microelectronic Engineering 163 (2016) 83–97. https://doi.org/10.1016/j.mee.2016.06.010.
[4] F.C. Ng, M.A. Abas, Underfill Flow in Flip-Chip Encapsulation Process: A Review, Journal of Electronic Packaging 144 (2022) 010803. https://doi.org/10.1115/1.4050697.
[5] J. Wang, Underfill of flip chip on organic substrate: viscosity, surface tension, and contact angle, Microelectronics Reliability 42 (2002) 293–299. https://doi.org/10.1016/S0026-2714(01)00231-1.


* micro-post arrays 
Study on the capillary performance of micro-post wicks with non-homogeneous configurations
https://link.springer.com/article/10.1007/s12541-016-0008-x
https://pubs.rsc.org/en/content/articlepdf/2018/lc/c8lc00458g

* finger-powered microfluidic system: Finger-Powered Microfluidic Systems Using Multilayer Soft Lithography and Injection Molding Processes